from bs4 import BeautifulSoup
from sys import argv
import boto3
import requests
import datetime


class TagParser:
    html_parser = "lxml"
    log_name = "tagparser.log"

    def __init__(self, url=None):
        self.url = url
        self.soup = None

        if url:
            self.load_page(url)

    def load_page(self, url: str):
        try:
            response = requests.get(url)
            self.url = url
            self.soup = BeautifulSoup(response.text, self.html_parser)
        except requests.ConnectionError as e:
            print("Error while getting page content:\n", e)

    @property
    def tags(self):
        dict_tags = {}
        if self.soup:
            for tag in self.soup.find_all():
                add_tag = dict.fromkeys([tag.name], dict_tags.get(tag.name, 0) + 1)
                dict_tags.update(add_tag)
        return dict_tags

    @property
    def tags_count(self):
        return sum(self.tags.values())

    def _generate_log(self, save_to_file: bool):
        now = datetime.datetime.now().strftime("%Y/%m/%d %H:%M")
        if save_to_file:
            with open(self.log_name, "a") as log_file:
                print(now, self.url, self.tags_count, self.tags, sep='\t', file=log_file)
        else:
            print(now, self.url, self.tags_count, self.tags, sep='\t')

    def save_log(self):
        self._generate_log(True)

    def print_log(self):
        self._generate_log(False)

    def upload_log(self, bucket_name: str):
        try:
            s3 = boto3.resource("s3")
            s3.Bucket(bucket_name).upload_file(self.log_name, self.log_name)
        except boto3.exceptions.S3UploadFailedError as e:
            print("Error while uploading log file to S3:\n", e)


if __name__ == "__main__":

    if len(argv) < 3:
        print("Please, run this program as 'tag_parser.py PAGE_URL BUCKET_NAME'")
        exit(1)
    page_url = argv[1]
    bucket = argv[2]

    my_parser = TagParser(page_url)
    my_parser.print_log()
    my_parser.save_log()
    my_parser.upload_log(bucket)
